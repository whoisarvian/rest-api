<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Auth extends BD_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        header("Access-Control-Allow-Origin: *");
        $this->methods['users_get']['limit'] = 10000; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 10000; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 500; // 50 requests per hour per user/key
        $this->kunci='34242342343244';
        $this->appUrl='https://localhost/restfull-api/';
        date_default_timezone_set('Asia/Jakarta');
    }

    public function status_get()
    {
        $this->auth();
        $this->response('OK');
    }

    public function login_post()
    {
        $email = $this->post('email'); //Username Posted
        $p = md5($this->post('password')); //Pasword Posted
        $q = array('email' => $email,'password'=>$p);

        $val = $this->db->get_where('users',$q)->row();
        
        if($this->db->get_where('users',$q)->num_rows()==0)
        {
            $this->response(['status' => 'not_match'], REST_Controller::HTTP_NOT_FOUND);
        }else if($val->password==$p && $val->email==$email)
        {
            if($val->is_active == 0) {
                $this->response(['status' => 'nonaktif'], REST_Controller::HTTP_NOT_FOUND);
            } else {
                $output=$this->createToken($val);
                $this->set_response($output, REST_Controller::HTTP_OK);
            }
        }else{
            $this->response(['status' => 'invalid'], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function createToken($val)
    {
        date_default_timezone_set('Asia/Jakarta');
        $date = new DateTime();
        $role=$val->role;
        $is_active=$val->is_active;
    	$kunci =$this->config->item('thekey');
        $token['role']=$role;
        $token['is_active']=$is_active;
        $token['id']=$val->id;
        $token['email']=$val->email;
        $token['iat'] = $date->getTimestamp();
        $token['exp'] = $date->getTimestamp() + (60*24*60); //set to 24 hour active
        $output['jwt'] = JWT::encode($token,$kunci ); //This is the output token
        $output['exp']=$token['exp'];
        $output['id']=$val->id;
        $output['email']=$val->email;
        $output['role']=$role;
        $output['is_active']=$is_active;
        return $output;
    }

    public function register_post()
    {
        date_default_timezone_set('Asia/Jakarta');
        $date = new DateTime();
        $email = $this->post('email'); //email Posted
        $name = $this->post('name'); //username Posted
        $phone = $this->post('phone'); //phone Posted
        $role = $this->post('role'); //role Posted
        $address = $this->post('address'); //address Posted
        $p = md5($this->post('password')); //Pasword Posted
        
        $emailArray = explode("@", $email);
        if( count($emailArray) != 2) $res = $this->response('email_invalid');
        
        // Validate the post data
        if(!empty($email) && !empty($name) && !empty($p)){
                    
            // Check if the given email already exists
            $cekUser =  $this->db->query("SELECT * FROM users where email = '$email'");
            if($cekUser->num_rows() > 0){
                // Set the response and exit
                $this->response(['status' => 'exist'], REST_Controller::HTTP_NOT_FOUND);
            }else{
                // Insert user data
                $userData = array(
                    'email'=>$email,
                    'name'=>$name,
                    'phone'=>$phone,
                    'address'=>$address,
                    'role'=>$role,
                    'image'=>null,
                    'date_created' => $date->getTimestamp(),
                    'is_active'=>'1',
                    'password'=>$p
                );
                $insert = $this->db->insert('users', $userData);
                
                // Check if the user data is inserted
                if($insert){
                    // Set the response and exit
                    $this->response([
                        'status' => TRUE,
                        'message' => 'The user has been added successfully.',
                        'data' => $insert
                    ], REST_Controller::HTTP_OK);
                }else{
                    // Set the response and exit
                    $this->response("Some problems occurred, please try again.", REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }else{
            // Set the response and exit
            $this->response(['status' => 'Provide complete user info to add.'], REST_Controller::HTTP_BAD_REQUEST);
        }
        
    }
    
    public function reset_get()
    {
        $token=$this->get('token');
        $t=$this->db->get_where('users',array('token'=>$token));
        if($t->num_rows() == 1)
        {
            $res=array('email'=>$t->row()->email);
            $this->response($res);
        }else{
            $res=false;
             $this->response($res,404);
        }
    }
    
    public function reset_put()
    {
        $p=$this->put('password');
        $email=$this->put('email');
        $this->db->where('email',$email);
        $res=$this->db->update('users',array('password'=>md5($p), 'token'=>null));
        $this->response($res);
    }
    
    public function reset_post()
    {
         $email = $this->post('email');
         $cek=$this->db->get_where('users',array('email'=>$email));
         if($cek->num_rows() > 0)
         {
             $user=$cek->row();
             $token=uniqid();
             $this->db->where('email',$email);
             $this->db->update('users',array('token'=>$token));
             $subject='Pembaruan Password Pramagang';
             $content='<p>';
             $content.=$user->name;
             $content.='</p>';
             $content.='<p>';
             $content.='Anda baru saja meminta pembaruan kata sandi. Klik tautan berikut jika memang benar Anda ingin melakukannya.';
             $content.='</p>';
             $content.='<p>';
             $resetUrl=$this->appUrl.'reset/'.$token;
             $content.='<a href="'.$resetUrl.'">'.$resetUrl.'</a>';
             $content.='</p>';
             $res=$this->sendmail($user->email, $subject, $content, '');
             $this->response(true);
         }else{
             $this->response(false,404);
         }
    }
    
    function index_put()
    {
        $this->auth();
        $db=$this->uri->segment('2');
        $id=$this->uri->segment('3');
        $data=$this->put();
        if($id!=null)
        {
            //$this->db->set($data);
            $this->db->where('id',$id);
            $this->db->update($db,$data);
    		$this->response('OK');
        }else{
            $this->response('FAILURE');
        }
    }
    
    function index_post()
    {
        $this->auth();
        $db=$this->uri->segment('2');
        $data=$this->post();
        $data['password'] = md5($data['password']);
        $this->db->insert($db,$data);
        $last = $this->db->order_by('id',"desc")
		->limit(1)
		->get($db)
		->row();
		$this->response($last);
    }
    
    public function sendmail($emailTo, $subject, $htmlContent,$attach){
        //data
       
		$this->load->library('email');
	    $config['protocol'] ='smtp';
		$config['smtp_host']='ssl://domain.co.id';
		$config['smtp_user']='pramagang@domain.co.id';
		$config['smtp_pass']=',)v8Ua_6pn]E';
		$config['smtp_port']='465';
		$config['smtp_timeout']=6;
		$config['charset'] = 'utf-8';
		$config['newline'] = "\r\n";
		$config['mailtype'] = 'html';
		$config['validation'] = TRUE;
		
		$this->email->initialize($config);		
		$this->email->from('pramagang@domain.co.id', 'pramagang');
		$this->email->to($emailTo);	
		
		$this->email->subject($subject);
		$this->email->message($htmlContent );
		
	
		$res=$this->email->send();
		if(!$res)
		{ 
		   	return false;
		}else {
    		//save to database
    		$dd=array('email'=>$emailTo,'subject'=>$subject,'content'=>$htmlContent,'attachment'=>$attach);
    		$this->db->insert('log_email',$dd);
    		return true;
		}
	}
    
}
