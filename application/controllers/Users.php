<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Users extends BD_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        header("Access-Control-Allow-Origin: *");
        $this->methods['users_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 1000; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 500; // 50 requests per hour per user/key
        $this->kunci='34242342343244';
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('users_model');
    }
    
    function me_get()
    {
        $this->auth();
        $email = $this->user_data->email;
        $q = $this->db->query("SELECT id, email, name, role, image, address, phone, is_active FROM users WHERE email = '$email'");
        if($q->num_rows() > 0)
        {
            $user = $q->row();
            $this->response($user);
        }else{
            $user = (object)array();
            $this->response('NOT_FOUND',500);
        }
    }
    
    function index_get()
    {
        $this->auth();
        $id=$this->uri->segment('2');
        $role = $this->user_data->role;
        if($role == 'superadmin')
        { 
            if($id != null) {
                $q = $this->db->query("SELECT id, email, name, role, image, address, phone, is_active FROM users WHERE id = '$id'");
                $user = $q->row();
                $this->response($user);
            } else {
                $data = $this->db->get('users');
                $this->response($data->result());
            }
        } else {
            if($id != null) {
                $q = $this->db->query("SELECT id, email, name, role, image, address, phone, is_active FROM users WHERE id = '$id'");
                $user = $q->row();
                $this->response($user);
            } else {
                $this->response('NOT_FOUND',500);
            }
        }
    }
    
    
    function index_put()
    {
        $this->auth();
        $id=$this->uri->segment('2');
        $data=$this->put();
        $result = $this->users_model->update($data, $id);
        if($result == true) {
            return $this->response([
                'status' => TRUE,
                'message' => 'The user has been updated successfully.',
                'data' => $data
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'User ID not found.'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    
    function changePass_put()
    {
        $this->auth();
        $id=$this->uri->segment('3');
        $data=$this->put();
        $result = $this->users_model->changePass($data, $id);
        $this->response($result);
    }
    
    function find_get()
    {
        $id = $this->uri->segment(3);
        $result = $this->users_model->get_one($id);
        $this->response($result);
    }
    
    function index_delete()
    {
        $this->auth();
        $id=$this->uri->segment('2');
        $result = $this->users_model->delete($id);
        if($result == true) {
            return $this->response([
                'status' => TRUE,
                'message' => 'The user has been deleted successfully.'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'User ID not found.'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    
    function uploadfoto_put()
    {
        $this->auth();
        $id=$this->uri->segment('3');
        $data=$this->put();
	    $img = $data['image'];
        $img = str_replace('data:image/png;base64,', '', $img);
	    $img = str_replace(' ', '+', $img);
        $image = base64_decode($img);
        $rand=uniqid();
        $filename = $id.'_'.$rand.'.' . 'png';
        //rename file name with random number
        $path = "./photos/users/";
        //image uploading folder path
        file_put_contents($path . $filename, $image);
        // image is bind and upload to respective folder
        // $data = array('image'=>$filename);
        // $result = $this->products_model->update($data, $id);
        $this->response($filename);
    }
}