<?php
class Users_model extends CI_Model {
    public $readable = '
        users.id,
        users.name, 
        users.email,
        users.image,
        users.address,
        users.phone';

    public function get_all($params, $user_data)
    {
        $readable = $this->readable;
        $this->db->select($readable);
        
        $this->db->from('users');
        
        //limit dan offset
        if($params['limit'] != null) 
        {
            $limit = $params['limit'];
            $this->db->limit($limit);
        }
        
        //order by
        $this->db->order_by('created_at','DESC');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function get_all_view($params)
    {
        
        $this->db->limit(50);
        $query = $this->db->get('users');
        
        //$query = $this->db->query("SELECT TOP 100 * FROM users");
        return $query->result();
    }
    
    public function get_one($id)
    {
        $q = $this->db->query("SELECT email, name, role, image, address, phone, is_active FROM users WHERE id = '$id'");
        if($q->num_rows() > 0)
        {
            $user = $q->row();
        }else{
            $user = (object)array();
        }
        return $user;
    }
    
    public function add($data, $user)
    {
        $data['created_by'] = $user->email;
        return $this->db->insert('users',$data);
    }
    
    public function update($data, $id)
    {
        if($id == null) return false;
        $this->db->where('id',$id);
        return $this->db->update('users',$data);
    }
    
    public function changePass($data, $id)
    {
        if($id == null) return false;
        $this->db->where('id',$id);
        $p=$data['password'];
        $email=$data['email'];
        $this->db->where('email',$email);
        return $this->db->update('users',array('password'=>md5($p)));
    }
    
    public function delete($id)
    {
        if($id == null) return false;
        $this->db->where('id',$id);
        return $this->db->delete('users');
    }
}